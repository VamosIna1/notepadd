package com.example.mynotes

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mynotes.model.NotesTable
import com.example.mynotes.room.NotesDatabase
import kotlinx.android.synthetic.main.activity_notes.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch


class NotesActivity:AppCompatActivity() {
    private var mDBTask: NotesDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes)
        mDBTask = NotesDatabase.getInstance(this)
//        setSupportActionBar(toolbar)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater:MenuInflater=menuInflater
        inflater.inflate(R.menu.item_notes, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menuSave -> {
                val title = edtxtTaskTitle.text.toString()
                val description = edtxDescription.text.toString()
                GlobalScope.launch {
                    val result =  mDBTask?.NotesDao()?.insertTask(NotesTable(null,title,description))
                    result?.let {
                        if (it != 0.toLong() ){
                            runOnUiThread {
                                edtxtTaskTitle.setText("")
                                edtxDescription.setText("")
                                Toast.makeText(this@NotesActivity,"Berhasil Input Data", Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}


