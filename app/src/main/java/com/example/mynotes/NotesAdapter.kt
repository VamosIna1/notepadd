package com.example.mynotes

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mynotes.model.NotesTable
import kotlinx.android.synthetic.main.list_item_notes.view.*

class NotesAdapter (val listNotes :
//                    Int
                    List<NotesTable>,var context: Context) : RecyclerView.Adapter<NotesAdapter.ListTaskViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListTaskViewHolder {
        return ListTaskViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_notes, parent, false))
    }

    override fun getItemCount(): Int {
        return listNotes.size
    }

    override fun onBindViewHolder(holder: ListTaskViewHolder, position: Int) {
        val listTask = listNotes.get(position)

        listTask.let {
            holder.title.text = it.title
            holder.description.text = it.description
        }


        holder.itemView.setOnClickListener {
            val intent = Intent(context, NotesUpdate::class.java)
            intent.putExtra("data",listNotes[position])
            context.startActivity(intent)
        }
//        when(position != 0){
//            (position % 3 == 0 && position % 5 == 0) -> holder.itemView.titleList.text = "BootCamp"
//            position % 3 == 0 -> holder.itemView.titleList.text = "Boot"
//            position % 5 == 0 -> holder.itemView.titleList.text = "camp"
//            else -> holder.itemView.titleList.text = position.toString()
//        }
    }

    inner class ListTaskViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val title = view.titleList
        val description = view.descriptionList
        val test = view.titleList
    }
}
