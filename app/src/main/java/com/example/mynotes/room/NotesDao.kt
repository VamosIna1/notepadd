package com.example.mynotes.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Update
import com.example.mynotes.model.NotesTable


@Dao
interface NotesDao {
    @Query("SELECT * FROM NotesTable ORDER BY id DESC ")
    fun getAllTask() : List<NotesTable>

    @Insert(onConflict = REPLACE)
    fun insertTask(notesTable: NotesTable)  : Long

    @Update
    fun updateNotes(notesTable: NotesTable)  : Int




}