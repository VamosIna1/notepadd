package com.example.mynotes

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mynotes.room.NotesDatabase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_notes.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private var mDBTask: NotesDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mDBTask = NotesDatabase.getInstance(this)
//        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        getData()
        addFloating.setOnClickListener {
            val i = Intent(this, NotesActivity::class.java)
            startActivity(i)
        }
    }

    override fun onResume() {
        super.onResume()
        swipeUpdate.setOnRefreshListener {
            getData()
            swipeUpdate.isRefreshing=false
        }
    }

    fun getData(){
        GlobalScope.launch {
            val listTask = mDBTask?.NotesDao()?.getAllTask()

            runOnUiThread {
                listTask?.let{
                    val adapter = NotesAdapter(it, this@MainActivity)
                    recyclerView.adapter = adapter
                    recyclerView.setHasFixedSize(true)
                }
            }
        }
    }

}
