package com.example.mynotes.warmup

fun main(){
val result =  akuTauNamanya(100)
    println(result)
}

fun akuTauNamanya(i :Int) {
    for (i in 1..100) {
        when {
            (i % 3 == 0 && i % 5 == 0) -> println("BootCamp")
            i % 3 == 0 -> println("Boot")
            i % 5 == 0 -> println("Camp")
            else -> println("$i")
        }
    }
}