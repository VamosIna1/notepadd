
package com.example.mynotes
import com.example.mynotes.R

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mynotes.model.NotesTable
import com.example.mynotes.room.NotesDatabase
import kotlinx.android.synthetic.main.activity_notes.*
import kotlinx.android.synthetic.main.activity_update_notes.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class NotesUpdate:AppCompatActivity() {
    private var mDBTask: NotesDatabase? = null
    val data:NotesTable? by lazy {
        intent.getParcelableExtra<NotesTable>("data")
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_notes)
        data?.let{
           edtxtTaskTitleU.setText(it.title)
            edtxDescriptionU.setText(it.description)
        }
        mDBTask = NotesDatabase.getInstance(this)

        btn_show_main.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater =menuInflater
        inflater.inflate(R.menu.item_notes, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menuSave -> {
                val titleUpdate = edtxtTaskTitleU.text.toString()
                val descriptionUpdate = edtxDescriptionU.text.toString()

                GlobalScope.launch {
                    val result =  mDBTask?.NotesDao()?.updateNotes(NotesTable(data?.id,titleUpdate,descriptionUpdate))
                    runOnUiThread {
                        if (result != 0){
                            edtxtTaskTitleU.setText("")
                            edtxDescriptionU.setText("")
                            Toast.makeText(this@NotesUpdate, "Update Berhasil untuk id ${data?.id}", Toast.LENGTH_LONG).show()
                            Log.d("data", data.toString())
                        }
                    }
                }
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
